from django.apps import AppConfig


class FooAppConfig(AppConfig):
    name = 'foo'
